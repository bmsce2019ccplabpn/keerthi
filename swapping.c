#include<stdio.h>
void swap(int*,int*);
int main()
{
    int a,b;
    printf("enter first number:");
    scanf("%d",&a);
    printf("enter second number:");
    scanf("%d",&b);
    printf("numbers before swapping are %d and %d",a,b);
    swap(&a,&b);
    printf("numbers after swapping are %d and %d",a,b);
    return 0;
}
void swap(int*a,int*b)
{  
    int c;
    c=*a;
    *a=*b;
    *b=c;
    
}